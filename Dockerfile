FROM nginx:1.23.4-bullseye

MAINTAINER Warren Roque <wroquem@gmail.com>

COPY training-app-web /usr/share/nginx/html/training-app-web
COPY default.conf /etc/nginx/conf.d